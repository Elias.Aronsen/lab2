package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

	private int n;

	/**
	 * If list is empty, head == null
	 * else head is the first element of the list.
	 */
	private Node<T> head;

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@Override
	public T get(int index) {
		return getNode(index).data;
	}

	/**
	 * Returns the node at the specified position in this list.
	 *
	 * @param index index of the node to return
	 * @return the node at the specified position in this list
	 * @throws IndexOutOfBoundsException if the index is out of range
	 *                                   ({@code index < 0 || index >= size()})
	 */
	private Node<T> getNode(int index) {

		if (index >= size() || index < 0) {
			throw new IndexOutOfBoundsException();
		}
		Node<T> currentNode = head;

		for (int i = 0; i <= index - 1; i++) {
			currentNode = currentNode.next;
		}

		return currentNode;
	}

	@Override
	public void add(int index, T element) {
		if (index > size() || index < 0) {
			throw new IndexOutOfBoundsException();
		}
		if (head == null) {
			head = new Node<T>(element);
			n++;
			return;
		}

		// adds new node at last in list
		else if (index == size()) {
			Node<T> currentNode = head;
			while (currentNode.next != null) {
				currentNode = currentNode.next;
			}
			currentNode.next = new Node<T>(element);
			n++;
			return;
		}

		// adds new node as new head
		else if (index == 0) {
			Node<T> currentNode = new Node<T>(element);
			currentNode.next = head;
			head = currentNode;
			n++;
			return;
		}

		// adds new node in specified index
		else {
			Node<T> currentNode = head;
			Node<T> newNode = new Node<T>(element);
			int i = 0;
			while (i < index - 1) {
				currentNode = currentNode.next;
				i++;
			}
			newNode.next = currentNode.next;
			currentNode.next = newNode;
			n++;
			return;
		}

	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n * 3 + 2);
		str.append("[");
		Node<T> currentNode = head;
		while (currentNode.next != null) {
			str.append(currentNode.data);
			str.append(", ");
			currentNode = currentNode.next;
		}
		str.append((T) currentNode.data);
		str.append("]");
		return str.toString();
	}

	@SuppressWarnings("hiding")
	private class Node<T> {
		T data;
		Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	}

}